module github.com/evatix-go/rediswrapper

go 1.15

require (
	github.com/bxcodec/faker/v3 v3.5.0
	github.com/go-redis/redis/v8 v8.3.0
	gitlab.com/evatix-go/core v0.8.1
	gitlab.com/evatix-go/errorwrapper v0.7.8
)
